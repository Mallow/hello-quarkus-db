package com.example;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonObject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class OrdersEndpointTest {
    @Test
    public void testOrderService() {

        // test @GET
        given()
                .when().get("/customers")
                .then()
                .statusCode(200)
                .body("$.size()", is(2));

        // creo un obj Order
        JsonObject objOrder = Json.createObjectBuilder()
                .add("items", "pc")
                .add("price", new Long(100))
                .build();


        // Test @POST al path "/orders/1 @PathParameter : customer Long id
        given()
                .contentType("application/json")
                .body(objOrder.toString())
                .when()
                .post("/orders/2")
                .then()
                .statusCode(201);

        // Creo un nuovo ordine per @PathParameter : customer Long id
        objOrder = Json.createObjectBuilder()
                .add("id", new Long(1))
                .add("items", "computer")
                .add("price", new Long(100))
                .build();

        // Test UPDATE
        given()
                .contentType("application/json")
                .body(objOrder.toString())
                .when()
                .put("/orders")
                .then()
                .statusCode(204);

        // Test GET al path "/orders/1
        given()
                .when().get("/orders?customerId=2")
                .then()
                .statusCode(200)
                .body(containsString("computer"));

        // Test DELETE Order #1
        given()
                .when().delete("/orders/1")
                .then()
                .statusCode(204);
    }
}
