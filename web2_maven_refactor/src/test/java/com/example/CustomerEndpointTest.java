package com.example;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonObject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class CustomerEndpointTest {

    @Test
    public void testCustomerService() {


        //test @GET
        given()
                .when().get("/customers")
                .then()
                .statusCode(200)
                .body("$.size()", is(2));


        //cro oggetto json per il post

        JsonObject obj = Json.createObjectBuilder()
                .add("name", "Francesco")
                .add("surname", "totti")
                .build();

        //test @POST
        given()
                .contentType("application/json")
                .body(obj.toString())
                .when()
                .post("/customers")
                .then()
                .statusCode(201);

        JsonObject objUpdate = Json.createObjectBuilder()
                .add("id", new Long(1))
                .add("name", "Marco")
                .add("surname", "Villa")
                .build();

        //test @PUT
        given()
                .contentType("application/json")
                .body(objUpdate.toString())
                .when()
                .put("/customers")
                .then()
                .statusCode(204);

        //test @DELETE
        given()
                .contentType("application/json")
                .when().delete("/customers?id=1")
                .then()
                .statusCode(204);


    }
}
