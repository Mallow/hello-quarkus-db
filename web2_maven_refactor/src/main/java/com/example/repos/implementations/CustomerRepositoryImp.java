package com.example.repos.implementations;

import com.example.exception.UserNotFoundException;
import com.example.model.Customer;
import com.example.repos.interfaces.CustomerRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.List;

@ApplicationScoped
public class CustomerRepositoryImp implements CustomerRepository {

    @Inject
    EntityManager em;

    @Override
    public List<Customer> findAll() {
        return em.createNamedQuery("Customers.findAll", Customer.class)
                .getResultList();
    }

    @Override
    public Customer findCustomerById(Long id) throws WebApplicationException {
        Customer c = em.find(Customer.class, id);
        if (c == null) {
            throw new WebApplicationException("customer not exist", 404);
        }
        return c;
    }

    @Transactional
    @Override
    public void updateCustomer(Customer customer) throws UserNotFoundException {
        Customer customerUpdated = findCustomerById(customer.getId());
        customerUpdated.setName(customer.getName());
        customerUpdated.setSurname(customer.getSurname());
    }

    @Transactional
    @Override
    public void createCustomer(Customer customer) {
        em.persist(customer);
    }

    @Transactional
    @Override
    public void deleteCustomer(Long customerId) throws UserNotFoundException {
        em.remove(findCustomerById(customerId));
    }
}
