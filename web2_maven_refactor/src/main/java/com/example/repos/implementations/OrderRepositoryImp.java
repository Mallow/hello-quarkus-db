package com.example.repos.implementations;

import com.example.model.Customer;
import com.example.model.Orders;
import com.example.repos.interfaces.OrderRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.List;

@ApplicationScoped
public class OrderRepositoryImp implements OrderRepository {

    @Inject
    EntityManager em;


    @Override
    public List<Orders> findAll(Long customerId) {
        return (List<Orders>) em.createNamedQuery("Orders.findAll")
                .setParameter("customerId", customerId)
                .getResultList();
    }

    @Override
    public Orders findOrderById(Long id) {
        Orders o = em.find(Orders.class, id);
        if (o == null)
            throw new WebApplicationException("Order non found", 404);

        return o;
    }

    @Transactional
    @Override
    public void updateOrder(Orders order) {
        Orders orderToUpdate = findOrderById(order.getId());
        orderToUpdate.setItems(order.getItems());
        orderToUpdate.setPrice(order.getPrice());
    }

    @Transactional
    @Override
    public void createOrder(Orders order, Customer customer) {
        order.setCustomer(customer);
        em.merge(order);
    }

    @Transactional
    @Override
    public void deleteOrder(Long orderId) {
        em.remove(findOrderById(orderId));
    }
}
