package com.example.repos.interfaces;

import com.example.exception.UserNotFoundException;
import com.example.model.Customer;

import javax.ws.rs.WebApplicationException;
import java.util.List;

public interface CustomerRepository {

    List<Customer> findAll();

    Customer findCustomerById(Long id) throws WebApplicationException;

    void updateCustomer(Customer customer) throws UserNotFoundException;

    void createCustomer(Customer customer);

    void deleteCustomer(Long customerId) throws UserNotFoundException;
}
