package com.example.repos.interfaces;

import com.example.model.Customer;
import com.example.model.Orders;

import java.util.List;

public interface OrderRepository {
    List<Orders> findAll(Long customerId);

    Orders findOrderById(Long id);

    void updateOrder(Orders order);

    void createOrder(Orders order, Customer customer);

    void deleteOrder(Long orderId);
}
