package com.example.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQuery(name = "Orders.findAll",
        query = "SELECT o FROM Orders o WHERE o.customer.id = :customerId ORDER BY o.items")
@Table(name = "orders", schema = "public")
public class Orders {
    @Id
    @SequenceGenerator(
            name = "orderSequence",
            sequenceName = "orderId_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orderSequence")
    private Long id;

    @Column(name = "items", length = 40)
    private String items;

    @Column(name = "price")
    private Long price;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonbTransient
    private Customer customer;

    public Orders() {

    }

    public Orders(String items, Long price) {
        this.items = items;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Orders)) return false;
        Orders orders = (Orders) o;
        return getId().equals(orders.getId()) &&
                getItems().equals(orders.getItems()) &&
                getPrice().equals(orders.getPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getItems(), getPrice());
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", items='" + items + '\'' +
                ", price=" + price +
                ", customer=" + customer +
                '}';
    }
}
