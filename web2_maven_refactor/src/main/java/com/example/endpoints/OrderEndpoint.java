package com.example.endpoints;

import com.example.model.Customer;
import com.example.model.Orders;
import com.example.repos.interfaces.CustomerRepository;
import com.example.repos.interfaces.OrderRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/orders")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class OrderEndpoint {

    @Inject
    OrderRepository orderRepository;
    @Inject
    CustomerRepository customerRepository;

    /*private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;

    @Inject
    public OrderEndpoint(OrderRepository orderRepository, CustomerRepository customerRepository) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
    }*/

    @GET
    public List<Orders> getAll(@QueryParam("customerId") Long id) {
        return orderRepository.findAll(id);
    }

    @POST
    @Path("/{customer}")
    public Response create(Orders order, @PathParam("customer") Long id) {
        Customer c = customerRepository.findCustomerById(id);
        orderRepository.createOrder(order, c);
        return Response.status(201).build();
    }

    @PUT
    public Response update(Orders order) {
        orderRepository.updateOrder(order);
        return Response.status(204).build();
    }

    @DELETE
    @Path("/{order}")
    public Response delete(@PathParam("order") Long id) {
        orderRepository.deleteOrder(id);
        return Response.status(204).build();
    }

}
