package com.example.endpoints;

import com.example.exception.UserNotFoundException;
import com.example.model.Customer;
import com.example.repos.implementations.CustomerRepositoryImp;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/customers")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class CustomerEndpoints {

    //private final CustomerRepository customerRepository;
    @Inject
    CustomerRepositoryImp customerRepository;

    /*@Inject
    public CustomerEndpoints(CustomerRepositoryImp customerRepository) {
        this.customerRepository = customerRepository;
    }*/

    @GET
    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    @POST
    public Response create(Customer customer) {
        customerRepository.createCustomer(customer);
        return Response.status(201).build();
    }

    @PUT
    public Response update(Customer customer) throws UserNotFoundException {
        customerRepository.updateCustomer(customer);
        return Response.status(204).build();
    }

    @DELETE
    public Response delete(@QueryParam("id") Long customerId) throws UserNotFoundException {
        customerRepository.deleteCustomer(customerId);
        return Response.status(204).build();
    }
}
