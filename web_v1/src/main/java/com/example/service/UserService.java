package com.example.service;

import com.example.model.User;
import com.example.exception.UserNotFoundException;

import java.util.List;

public interface UserService {
    User findUserById(long id) throws UserNotFoundException;

    List<User> getAllUser();

    User updateUser(long id, User user) throws UserNotFoundException;

    User saveUser(User user);

    void deleteUser(long id) throws UserNotFoundException;
}
