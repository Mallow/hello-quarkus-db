package com.example;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

@QuarkusTest
public class RestEndpointTest {
    @Test
    public void testUser() {
        given()
                .when().get("/persons")
                .then()
                .statusCode(500)
                .body(
                        containsString("luciano"),
                        containsString("stefano")
                );
    }

}
