package com.example.base;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

@ApplicationScoped
public class UserRepository {
    List<User> userList = new ArrayList<>();
    int iter;

    public int getNextUSerId() {
        return iter++;
    }

    /*public List<User> findAll() {
        return userList;
    }*/

    @PersistenceContext
    EntityManager em;

    /*public List<User> findAll() {
        return Arrays.asList(new User("luciano", "degni"),
        new User("stefano", "indelicato"),
        new User("emanuele", "pirola"));
    }*/
    public List<User> findAll() {
        return em.createQuery("select u from User u", User.class).getResultList();
    }

    public User findUserById(long id) {
        User tmp = new User();
        for (User u : userList) {
            if (u.getId() == id) {
                tmp = u;
            }
        }
        return tmp;
    }

    public void updateUser(User user) {
        if ((user.getName().length() > 0) && (user.getSurname().length() > 0)) {
            User userUpdated = findUserById(user.getId());
            userUpdated.setName(user.getName());
            userUpdated.setSurname(user.getSurname());
        }
    }

    public void createUser(User user) {
        user.setId(getNextUSerId());
        findAll().add(user);
    }

    public void deleteUser(long userId) {
        User u = findUserById(userId);
        findAll().remove(u);
    }
}
