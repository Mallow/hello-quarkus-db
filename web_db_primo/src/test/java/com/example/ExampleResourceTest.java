package com.example;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonObject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

@QuarkusTest
public class ExampleResourceTest {


    @Test
    public void testUser() {

        //creation json obj
        JsonObject obj = Json.createObjectBuilder()
                .add("name", "luciano")
                .add("surname", "degni").build();

        //test post
        given()
                .contentType("application/json")
                .body(obj.toString())
                .when()
                .post("/persons")
                .then()
                .statusCode(201);

        //test get
        given()
                .when().get("/persons")
                .then()
                .statusCode(200)
                .body(containsString("luciano"), containsString("degni"));

        obj = Json.createObjectBuilder()
                .add("id", "0")
                .add("name", "stefano")
                .add("surname", "indelicato").build();

        //test put
        given()
                .contentType("application/json")
                .body(obj.toString())
                .when()
                .put("/persons")
                .then()
                .statusCode(204);

        //test delete
        given()
                .contentType("application/json")
                .when()
                .delete("/persons?id=1")
                .then()
                .statusCode(500);
    }


}