package org.acme;

import java.util.Arrays;
import java.util.List;

public class UtilityUser {
    public List<User> buildUser() {
        return Arrays.asList(new User("luciano", "degni"),
                new User("stefano", "indelicato"),
                new User("emanuele", "pirola"));
    }
}
