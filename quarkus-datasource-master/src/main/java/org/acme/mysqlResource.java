package org.acme;

import io.agroal.api.AgroalDataSource;
import io.quarkus.agroal.DataSource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.*;
import java.util.List;

@Path("/mysqlResource")
public class mysqlResource {

    @Inject
    @DataSource("users")
    AgroalDataSource dataSource;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        Connection conn = null;
        Statement st = null;
        try {
            conn = dataSource.getConnection();
            st = conn.createStatement();

            UtilityUser u = new UtilityUser();

            List<User> users = u.buildUser();


            String s =  users.get(1).getName() ;
            String s1 = users.get(1).getSurname();
            String q1 = "insert into user (nome, cognome)  VALUES ('Stefanno', 'Indelicato')";
            String q2 = "select * from user";

            st.executeUpdate(q1);

            ResultSet rs = st.executeQuery(q2);
            while (rs.next())
                System.out.println(rs.getString(1) + "  " + rs.getString(2) + "  " + rs.getString(3) );

            st.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (st != null) st.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return "Query effettuates";
    }
}