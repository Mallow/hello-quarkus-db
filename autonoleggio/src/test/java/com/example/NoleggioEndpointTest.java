package com.example;

import com.example.utilityTest.UtilityPathTest;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class NoleggioEndpointTest {
    @Test
    public void testNoleggioService() {

        // @GET all'inizio nessun user ha ancora nessun noleggi
        given()
                .when().get(UtilityPathTest.PATH_NOLEGGIO_ID_UTENTE)
                .then()
                .statusCode(200)
                .body("$.size()", is(0));

        // creo un auto da noleggiare
        JsonObject objAuto = Json.createObjectBuilder()
                .add("colore", "rosso")
                .add("deleted", 0)
                .add("marca", "fiat")
                .add("modello", "500XL")
                .add("targa", "AA123BB")
                .build();


        // test @POST crazione auto
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(objAuto.toString())
                .when()
                .post(UtilityPathTest.PATH_CREATE_AUTO)
                .then()
                .statusCode(201);

        //test @POST creazione noleggio
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(UtilityPathTest.PATH_NOLEGGIO_CREAZIONE)
                .then()
                .statusCode(201);

        //@GET verifico risultato
        given()
                .when().get(UtilityPathTest.PATH_NOLEGGIO_ID_UTENTE)
                .then()
                .statusCode(200)
                .body("$.size()", is(1));


        //@DELETE
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().delete("/noleggi/1")
                .then()
                .statusCode(204);
    }
}
