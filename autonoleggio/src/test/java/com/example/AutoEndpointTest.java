package com.example;

import com.example.utilityTest.UtilityPathTest;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class AutoEndpointTest {
    @Test
    public void testAutoService() {

        //test @GET
        given()
                .when().get(UtilityPathTest.PATH_AUTO)
                .then()
                .statusCode(200)
                .body("$.size()", is(0));

        JsonObject objAuto = Json.createObjectBuilder()
                .add("colore", "rosso")
                .add("deleted", 0)
                .add("marca", "fiat")
                .add("modello", "500XL")
                .add("targa", "AA123BB")
                .build();


        // test @POST crazione auto
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(objAuto.toString())
                .when()
                .post(UtilityPathTest.PATH_CREATE_AUTO)
                .then()
                .statusCode(201);

        objAuto = Json.createObjectBuilder()
                .add("colore", "verde")
                .add("id", 1)
                .add("deleted", 0)
                .add("marca", "fiat")
                .add("modello", "500XL")
                .add("targa", "AA123BB")
                .build();

        //@PUT
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(objAuto.toString())
                .when()
                .put(UtilityPathTest.PATH_AUTO)
                .then()
                .statusCode(204);

        // @GET controllo se il body è cambiato ovvero ho aggiornato l'auto
        given()
                .when().get(UtilityPathTest.PATH_UPDATED_AUTO)
                .then()
                .statusCode(200)
                .body(containsString("verde"));

        //@DELETE
        given()
                .when().delete(UtilityPathTest.PATH_DELETE_AUTO)
                .then()
                .statusCode(204);
    }
}
