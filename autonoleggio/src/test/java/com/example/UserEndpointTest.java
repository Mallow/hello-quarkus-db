package com.example;


import com.example.utilityTest.UtilityPathTest;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class UserEndpointTest {
    @Test
    public void testUserService() {

        //test del @GET ancora nessun utente sul body del
        given()
                .when().get(UtilityPathTest.PATH_USER)
                .then()
                .statusCode(200)
                .body("$.size()", is(1));

        JsonObject objUser = Json.createObjectBuilder()
                .add("deleted", 0)
                .add("name", "luciano")
                .add("password", "ciccio2000")
                .add("surname", "degni")
                .add("username", "xelor")
                .build();

        //@POST creazione utente
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(objUser.toString())
                .when()
                .post(UtilityPathTest.PATH_USER)
                .then()
                .statusCode(201);


        // @POST login dell'utente
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(UtilityPathTest.PATH_USER_LOGIN)
                .then()
                .statusCode(200);

        //test @PUT
        //creo oggetto
        objUser = Json.createObjectBuilder()
                .add("id", new Long(1))
                .add("deleted", 0)
                .add("name", "christian")
                .add("password", "ciccio2000")
                .add("surname", "finucci")
                .add("username", "lollo")
                .build();

        //test del @PUT
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(objUser.toString())
                .when()
                .put(UtilityPathTest.PATH_USER)
                .then()
                .statusCode(204);


        // @POST registrazione
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(UtilityPathTest.PATH_USER_REGISTRAZIONE)
                .then()
                .statusCode(200);
    }
}
