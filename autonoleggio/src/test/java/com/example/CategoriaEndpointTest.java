package com.example;

import com.example.utilityTest.UtilityPathTest;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class CategoriaEndpointTest {
    @Test
    public void testCategoriaService() {


        //@GET
        given()
                .when().get(UtilityPathTest.PATH_CATEGORIA)
                .then()
                .statusCode(200)
                .body("$.size()", is(1));


        // @POST creazione Categoria
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(UtilityPathTest.PATH_CREATE_CATEGORIA)
                .then()
                .statusCode(201);


    }
}
