package com.example.utilityTest;

public class UtilityPathTest {
    /*------------------------------- User -----------------------------------------*/
    public static final String PATH_USER = "/user";
    public static final String PATH_USER_LOGIN = PATH_USER + "/login?username=xelor&password=ciccio2000";
    public static final String PATH_USER_REGISTRAZIONE = PATH_USER + "/registrazione?username=mallow&password=mypass&nome=stefano&cognome=indelicato";
    /*--------------------- Auto ------------------------------------*/
    public static final String PATH_AUTO = "/autos";
    // "/1" è l'id categoria passato come @ @PathParam
    public static final String PATH_CREATE_AUTO = PATH_AUTO + "/1";
    // "?id=1" id ell'auto da aggiornare passto come @QueryParam
    public static final String PATH_UPDATED_AUTO = PATH_AUTO + "?id=1";
    // "/1" è l'id categoria passato come @ @PathParam
    public static final String PATH_DELETE_AUTO = PATH_AUTO + "/1";
    /*------------- Categoria -------------------------------------------------*/
    public static final String PATH_CATEGORIA = "/categoria";
    public static final String PATH_CREATE_CATEGORIA = PATH_CATEGORIA + "/?descrizione=sportiva&prezzoGiornaliero=5.00&prezzoSettimanale=15.50&prezzoMensile=30.60";
    /*------------ Noleggio -----------------------------------*/
    public static final String PATH_NOLEGGIO = "/noleggi";
    public static final String PATH_NOLEGGIO_ID_UTENTE = PATH_NOLEGGIO + "/?userId=1";
    public static final String PATH_NOLEGGIO_CREAZIONE = PATH_NOLEGGIO + "/1/2/?dataFine=2020-07-01&dataInizio=2020-06-23&dataNoleggio=2020-06-20";

}
