package com.example.endpoints;

import com.example.model.Categoria;
import com.example.repos.interfaces.CategoriaRepository;
import com.example.utility.UtilityFile;
import com.example.utility.UtilityPath;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path(UtilityPath.PATH_CATEGORIA)
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class CategoriaEndpoint {

    @Inject
    CategoriaRepository categoriaRepository;

    @GET
    public List<Categoria> getAll() {
        return categoriaRepository.findAllCategorie();
    }

    @POST
    public Response create(@QueryParam("descrizione") String desc, @QueryParam("prezzoGiornaliero") String prezzoG,
                           @QueryParam("prezzoSettimanale") String prezzoS, @QueryParam("prezzoMensile") String prezzoM) {
        float pG = Float.parseFloat(prezzoG);
        float pS = Float.parseFloat(prezzoS);
        float pM = Float.parseFloat(prezzoM);
        Categoria c = new Categoria(desc, pG, pM, pS);
        categoriaRepository.createCategoria(c);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("create categoria" + "\n" + utilityFile.formatToJson(c) + "\ncreated!");
        return Response.status(201).build();
    }

    @POST
    public Response create(Categoria categoria) {
        categoriaRepository.createCategoria(categoria);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("create categoria from json" + "\n" + utilityFile.formatToJson(categoria) + "\ncreated!");
        return Response.status(201).build();
    }

    @PUT
    public Response update(Categoria c) {
        categoriaRepository.updateCategoria(c);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("update categoria" + "\n" + utilityFile.formatToJson(categoriaRepository.findCategoriaById(c.getId())) + "\nupdated!");
        return Response.status(204).build();
    }

    @DELETE
    public Response delete(@QueryParam("id") Long id) {
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write(utilityFile.formatToJson(categoriaRepository.findCategoriaById(id)) + "\ndeleted!");
        categoriaRepository.deleteCategoria(id);
        return Response.status(204).build();
    }


}
