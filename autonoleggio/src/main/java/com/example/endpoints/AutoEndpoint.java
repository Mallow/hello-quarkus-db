package com.example.endpoints;

import com.example.model.Auto;
import com.example.model.Categoria;
import com.example.repos.interfaces.AutoRepository;
import com.example.repos.interfaces.CategoriaRepository;
import com.example.utility.UtilityFile;
import com.example.utility.UtilityPath;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path(UtilityPath.PATH_AUTO)
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AutoEndpoint {

    @Inject
    AutoRepository autoRepository;

    @Inject
    CategoriaRepository categoriaRepository;

    @GET
    public List<Auto> getAll() {
        List<Auto> listAuto = autoRepository.findAllAuto();
        listAuto.forEach( auto -> {
            UtilityFile utilityFile = new UtilityFile();
            utilityFile.write("get auto" + "\n" + utilityFile.formatToJson(auto));
        });
        return listAuto;
    }

    @POST
    @Path("/{category}")
    public Response create(Auto auto, @PathParam("category") Long id) {
        Categoria c = categoriaRepository.findCategoriaById(id);
        autoRepository.createAuto(auto, c);
        return Response.status(201).build();
    }

    @PUT
    public Response update(Auto auto) {
        autoRepository.updateAuto(auto);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("update auto" + "\n" + utilityFile.formatToJson(autoRepository.findAutoById(auto.getId())) + "\nupdated!");
        return Response.status(204).build();
    }

    @DELETE
    @Path("/{auto}")
    public Response delete(@PathParam("auto") Long id) {
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("delete auto" + "\n" + utilityFile.formatToJson(autoRepository.findAutoById(id)) + "\ndeleted!");
        autoRepository.deleteAuto(id);
        return Response.status(204).build();
    }

}
