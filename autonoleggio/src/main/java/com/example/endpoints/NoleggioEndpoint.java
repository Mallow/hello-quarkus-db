package com.example.endpoints;

import com.example.model.Auto;
import com.example.model.Noleggio;
import com.example.model.User;
import com.example.repos.interfaces.AutoRepository;
import com.example.repos.interfaces.NoleggioRepository;
import com.example.repos.interfaces.UserRepository;
import com.example.utility.UtilityFile;
import com.example.utility.UtilityPath;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Path(UtilityPath.PATH_NOLEGGIO)
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class NoleggioEndpoint {

    @Inject
    AutoRepository autoRepository;

    @Inject
    NoleggioRepository noleggioRepository;

    @Inject
    UserRepository userRepository;

    @GET
    public List<Noleggio> getAllFromId(@QueryParam("userId") Long userId) {
        List<Noleggio> listNoleggi =  noleggioRepository.findAllNoleggi(userId);
        listNoleggi.forEach(noleggio -> {
            UtilityFile utilityFile = new UtilityFile();
            utilityFile.write("get noleggio by id = " + userId+  "\n" + utilityFile.formatToJson(noleggio));
        });
        return listNoleggi;
    }


    @POST
    @Path("/{user}/{auto}")
    public Response create(
            @QueryParam("dataFine") String dataFine,
            @QueryParam("dataInizio") String dataInizio,
            @QueryParam("dataNoleggio") String dataNoleggio,
            @PathParam("user") Long idUser,
            @PathParam("auto") Long idAuto) {
        Date dataInizioDate = null;
        Date dataFineDate = null;
        Date dataNoleggioDate = null;
        try {
            dataInizioDate = new SimpleDateFormat("yyyy-MM-dd").parse(dataInizio);
            dataFineDate = new SimpleDateFormat("yyyy-MM-dd").parse(dataFine);
            dataNoleggioDate = new SimpleDateFormat("yyyy-MM-dd").parse(dataNoleggio);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        User utente = userRepository.findUserById(idUser);
        Auto automobile = autoRepository.findAutoById(idAuto);
        Noleggio n = new Noleggio(dataInizioDate, dataFineDate, dataNoleggioDate, automobile, utente);
        noleggioRepository.createNoleggio(n, utente, automobile);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("create noleggio" + "\n" + utilityFile.formatToJson(n) + "\nupdated!");
        return Response.status(201).build();
    }

    @PUT
    public Response update(Noleggio noleggio) {
        noleggioRepository.updateNoleggio(noleggio);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("update noleggio" + "\n" + utilityFile.formatToJson(noleggioRepository.findNoleggioById(noleggio.getId())) + "\nupdated!");
        return Response.status(204).build();
    }


    @Path("/{noleggio}")
    @DELETE
    public Response delete(@PathParam("noleggio") Long id) {
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("delete noleggio" + "\n" + utilityFile.formatToJson(noleggioRepository.findNoleggioById(id)) + "\ndeleted!");
        noleggioRepository.deleteNoleggio(id);
        return Response.status(204).build();
    }

    @Path("/{auto}")
    @POST
    public Response noleggia(@PathParam("auto") Long idAuto, @QueryParam("dataInizio") String dataInizio, @QueryParam("dataFine") String dataFine, @QueryParam("username") String username) {
        Date dInizo = null;
        Date dFine = null;

        SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALY);

        try {
            dInizo = smf.parse(smf.format(dataInizio));
            dFine = smf.parse(smf.format(dataFine));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Auto a = autoRepository.findAutoById(idAuto);
        if (!noleggioRepository.autoPrenotata(a, dInizo, dFine)) {
            User u = userRepository.findUserById(userRepository.getUserIdByUsername(username));
            Date dataNoleggio = new Date();
            Noleggio noleggio = new Noleggio(dInizo, dFine, dataNoleggio, a, u);
            noleggioRepository.addNoleggio(noleggio);
        } else {
            return Response.status(404).build();
        }
        return Response.status(201).build();
    }
}
