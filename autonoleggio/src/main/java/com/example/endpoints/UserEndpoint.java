package com.example.endpoints;

import com.example.exception.UserNotFoundException;
import com.example.model.User;
import com.example.repos.interfaces.UserRepository;
import com.example.utility.UtilityFile;
import com.example.utility.UtilityPath;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path(UtilityPath.PATH_USER)
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class UserEndpoint {

    @Inject
    UserRepository userRepository;


    @GET
    public List<User> getAll() {
        return userRepository.findAllUser();
    }

    @POST
    public Response create(User user) {
        userRepository.createUser(user);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("create user" + "\n" + utilityFile.formatToJson(user) + "\ncreated!");
        return Response.status(201).build();
    }

    @PUT
    public Response update(User user) throws WebApplicationException, UserNotFoundException {
        userRepository.updateUser(user);
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("update user" + "\n" + utilityFile.formatToJson(userRepository.findUserById(user.getId())) + "\nupdated!");
        return Response.status(204).build();
    }

    @DELETE
    public Response delete(@QueryParam("id") Long userId) throws WebApplicationException, UserNotFoundException {
        UtilityFile utilityFile = new UtilityFile();
        utilityFile.write("delete user" + "\n" + utilityFile.formatToJson(userRepository.findUserById(userId)) + "\nupdated!");
        userRepository.deleteUser(userId);
        return Response.status(204).build();
    }

    @Path("/login")
    @POST
    public Response login(@QueryParam("username") String username, @QueryParam("password") String password) {
        userRepository.login(username, password);
        return Response.ok().build();
    }

    @Path("/registrazione")
    @POST
    public Response registrazione(
            @QueryParam("username") String username,
            @QueryParam("password") String password,
            @QueryParam("nome") String nome,
            @QueryParam("cognome") String cognome) {
        userRepository.registrazione(username, password, nome, cognome);
        return Response.ok().build();
    }
}
