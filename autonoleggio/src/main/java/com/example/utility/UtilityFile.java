package com.example.utility;

import com.example.model.Auto;
import com.example.model.Categoria;
import com.example.model.Noleggio;
import com.example.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import java.io.FileWriter;
import java.io.IOException;

@ApplicationScoped
public class UtilityFile {
    private static FileWriter fw;
    private static int static_counter = 0;
    private final int count;
    private final JsonbConfig config = new JsonbConfig()
            .withFormatting(true);
    private final Jsonb jsonb = JsonbBuilder.newBuilder()
            .withConfig(config).build();

    public UtilityFile() {
        count = static_counter++;
    }

    public void write(String jsonString) {
        try {
            fw = new FileWriter("C:\\Users\\Luciano Degni\\IdeaProjects\\autonoleggio\\reports\\report" + count + ".txt");
            fw.write(jsonString);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String formatToJson(Object o) {
        if (o != null) {
            if (o instanceof Categoria) {
                return jsonb.toJson((Categoria) o);
            } else if (o instanceof User) {
                return jsonb.toJson((User) o);
            } else if (o instanceof Auto) {
                return jsonb.toJson((Auto) o);
            } else if (o instanceof Noleggio) {
                return jsonb.toJson((Noleggio) o);
            }
        }
        return null;
    }
}
