package com.example.utility;

public class UtilityPath {
    public static final String PATH_AUTO = "/autos";
    public static final String PATH_CATEGORIA = "/categoria";
    public static final String PATH_USER = "/user";
    public static final String PATH_NOLEGGIO = "/noleggi";
}
