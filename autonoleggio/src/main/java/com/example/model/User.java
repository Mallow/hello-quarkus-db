package com.example.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
@Cacheable
@NamedQuery(name = "User.findAll",
        query = "SELECT u FROM User u ORDER BY  u.id",
        hints = @QueryHint(name = "org.hibernate.cacheable", value = "true"))
@Table(name = "user", schema = "public")
public class User {

    @Id
    @SequenceGenerator(
            name = "userSequence",
            sequenceName = "userId_seq",
            allocationSize = 1,
            initialValue = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userSequence")
    private Long id;

    @Column(name = "deleted", nullable = false, columnDefinition = "integer default 0 not null")
    private int deleted;
    @Column(name = "surname", nullable = false, length = 20)
    private String surname;
    @Column(name = "name", nullable = false, length = 20)
    private String name;
    @Column(name = "password", nullable = false, length = 20)
    @Size(min = 6, max = 15)
    //@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]) (?=.*[@#$%^&+=])(?=\\S+$).{8,20}$" )
    private String password;
    @Column(name = "username", nullable = false, length = 20, unique = true)
    private String username;


    @OneToMany(mappedBy = "user")
    @JsonbTransient
    private List<Noleggio> noleggios;

    public User() {
    }

    public User(int deleted, String surname, String name, String password, String username) {
        this.deleted = deleted;
        this.surname = surname;
        this.name = name;
        this.password = password;
        this.username = username;
    }

    public User(Long id, int deleted, String surname, String name, String password, String username) {
        this(deleted, surname, name, password, username);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Noleggio> getNoleggios() {
        return noleggios;
    }

    public void setNoleggios(List<Noleggio> noleggios) {
        this.noleggios = noleggios;
    }

    public Noleggio addNoleggio(Noleggio noleggio) {
        getNoleggios().add(noleggio);
        noleggio.setUser(this);
        return noleggio;
    }

    public Noleggio removeNoleggio(Noleggio noleggio) {
        getNoleggios().remove(noleggio);
        noleggio.setUser(null);
        return noleggio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getSurname().equals(user.getSurname()) &&
                getName().equals(user.getName()) &&
                getPassword().equals(user.getPassword()) &&
                getUsername().equals(user.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSurname(), getName(), getPassword(), getUsername());
    }
}
