package com.example.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.Objects;

@Entity
@Cacheable
@NamedQuery(name = "Categoria.findAll",
        query = "SELECT c FROM Categoria c ORDER BY  c.id",
        hints = @QueryHint(name = "org.hibernate.cacheable", value = "true"))
@Table(name = "Categoria", schema = "public")
public class Categoria {

    @Id
    @SequenceGenerator(
            name = "categoriaSequence",
            sequenceName = "categoriaId_seq",
            allocationSize = 1,
            initialValue = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categoriaSequence")
    private Long id;


    @Column(name = "deleted", nullable = false, columnDefinition = "integer default 0 not null")
    private int deleted;
    @Column(name = "descrizione")
    @Pattern(regexp = "[a-z]*[A-Z]*")
    private String descrizione;

    //@Pattern(regexp = "[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)")
    @Column(name = "prezzoGiornaliero")
    private float prezzoGiornaliero;

    //@Pattern(regexp = "[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)")
    @Column(name = "prezzoMensile")
    private float prezzoMensile;

    @Column(name = "prezzoSettimanale")
    //@Pattern(regexp = "[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)")
    private float prezzoSettimanale;

    @OneToMany(mappedBy = "categoria")
    @JsonbTransient
    private List<Auto> autos;

    public Categoria() {
    }

    public Categoria(int deleted, String descrizione, float prezzoGiornaliero, float prezzoMensile, float prezzoSettimanale) {
        this.deleted = deleted;
        this.descrizione = descrizione;
        this.prezzoGiornaliero = prezzoGiornaliero;
        this.prezzoMensile = prezzoMensile;
        this.prezzoSettimanale = prezzoSettimanale;
    }

    public Categoria(String descrizione, float prezzoGiornaliero, float prezzoMensile, float prezzoSettimanale) {
        this.descrizione = descrizione;
        this.prezzoGiornaliero = prezzoGiornaliero;
        this.prezzoMensile = prezzoMensile;
        this.prezzoSettimanale = prezzoSettimanale;
    }

    public Categoria(Long id, int deleted, String descrizione, float prezzoGiornaliero, float prezzoMensile, float prezzoSettimanale) {
        this(deleted, descrizione, prezzoGiornaliero, prezzoMensile, prezzoSettimanale);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public float getPrezzoGiornaliero() {
        return prezzoGiornaliero;
    }

    public void setPrezzoGiornaliero(float prezzoGiornaliero) {
        this.prezzoGiornaliero = prezzoGiornaliero;
    }

    public float getPrezzoMensile() {
        return prezzoMensile;
    }

    public void setPrezzoMensile(float prezzoMensile) {
        this.prezzoMensile = prezzoMensile;
    }

    public float getPrezzoSettimanale() {
        return prezzoSettimanale;
    }

    public void setPrezzoSettimanale(float prezzoSettimanale) {
        this.prezzoSettimanale = prezzoSettimanale;
    }

    public List<Auto> getAutos() {
        return autos;
    }


    public void setAutos(List<Auto> autos) {
        this.autos = autos;
    }

    public Auto removeAuto(Auto auto) {
        getAutos().remove(auto);
        auto.setCategoria(null);
        return auto;
    }

    public Auto addAuto(Auto auto) {
        getAutos().add(auto);
        auto.setCategoria(this);
        return auto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Categoria)) return false;
        Categoria categoria = (Categoria) o;
        return getDeleted() == categoria.getDeleted() &&
                Float.compare(categoria.getPrezzoGiornaliero(), getPrezzoGiornaliero()) == 0 &&
                Float.compare(categoria.getPrezzoMensile(), getPrezzoMensile()) == 0 &&
                Float.compare(categoria.getPrezzoSettimanale(), getPrezzoSettimanale()) == 0 &&
                getId().equals(categoria.getId()) &&
                Objects.equals(getDescrizione(), categoria.getDescrizione()) &&
                getAutos().equals(categoria.getAutos());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDeleted(), getDescrizione(), getPrezzoGiornaliero(), getPrezzoMensile(), getPrezzoSettimanale(), getAutos());
    }

    @Override
    public String toString() {
        return "Categoria{" +
                "id=" + id +
                ", deleted=" + deleted +
                ", descrizione='" + descrizione + '\'' +
                ", prezzoGiornaliero=" + prezzoGiornaliero +
                ", prezzoMensile=" + prezzoMensile +
                ", prezzoSettimanale=" + prezzoSettimanale +
                ", autos=" + autos +
                '}';
    }
}
