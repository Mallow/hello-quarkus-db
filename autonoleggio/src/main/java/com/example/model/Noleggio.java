package com.example.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Cacheable
@NamedQuery(name = "Noleggio.findAll",
        query = "SELECT n FROM Noleggio n WHERE n.user.id = :userId  ORDER BY  n.id",
        hints = @QueryHint(name = "org.hibernate.cacheable", value = "true"))
@Table(name = "noleggio", schema = "public")
public class Noleggio {

    @Id
    @SequenceGenerator(
            name = "noleggioSequence",
            sequenceName = "noleggioId_seq",
            allocationSize = 1,
            initialValue = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "noleggioSequence")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dataInizio")
    private Date dataInizio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dataFine")
    private Date dataFine;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dataNoleggio")
    private Date dataNoleggio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idAuto")
    @JsonbTransient
    private Auto auto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idUser")
    @JsonbTransient
    private User user;

    public Noleggio() {
    }

    public Noleggio(Date dataInizio, Date dataFine, Date dataNoleggio) {
        this.dataInizio = dataInizio;
        this.dataFine = dataFine;
        this.dataNoleggio = dataNoleggio;
    }

    public Noleggio(Date dataInizio, Date dataFine, Date dataNoleggio, Auto auto, User user) {
        this.dataInizio = dataInizio;
        this.dataFine = dataFine;
        this.dataNoleggio = dataNoleggio;
        this.auto = auto;
        this.user = user;
    }

    public Noleggio(Long id, Date dataInizio, Date dataFine, Date dataNoleggio, Auto auto, User user) {
        this(dataInizio, dataFine, dataNoleggio, auto, user);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(Date dataInizio) {
        this.dataInizio = dataInizio;
    }

    public Date getDataFine() {
        return dataFine;
    }

    public void setDataFine(Date dataFine) {
        this.dataFine = dataFine;
    }

    public Date getDataNoleggio() {
        return dataNoleggio;
    }

    public void setDataNoleggio(Date dataNoleggio) {
        this.dataNoleggio = dataNoleggio;
    }

    public Auto getAuto() {
        return auto;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Noleggio)) return false;
        Noleggio noleggio = (Noleggio) o;
        return getId().equals(noleggio.getId()) &&
                getDataInizio().equals(noleggio.getDataInizio()) &&
                getDataFine().equals(noleggio.getDataFine()) &&
                getDataNoleggio().equals(noleggio.getDataNoleggio()) &&
                getAuto().equals(noleggio.getAuto()) &&
                getUser().equals(noleggio.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDataInizio(), getDataFine(), getDataNoleggio(), getAuto(), getUser());
    }

    @Override
    public String toString() {
        return "Noleggio{" +
                "id=" + id +
                ", dataInizio=" + dataInizio +
                ", dataFine=" + dataFine +
                ", dataNoleggio=" + dataNoleggio +
                ", auto=" + auto +
                ", user=" + user +
                '}';
    }
}
