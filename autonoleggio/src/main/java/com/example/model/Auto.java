package com.example.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity
@Cacheable
@NamedQuery(name = "Auto.findAll",
        query = "SELECT a FROM Auto a ORDER BY  a.id",
        hints = @QueryHint(name = "org.hibernate.cacheable", value = "true"))
@Table(name = "auto", schema = "public")
public class Auto {

    @Id
    @SequenceGenerator(
            name = "autoSequence",
            sequenceName = "autoId_seq",
            allocationSize = 1,
            initialValue = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoSequence")
    private Long id;
    @Column(name = "deleted", nullable = false, columnDefinition = "integer default 0 not null")
    private int deleted;

    @Column(name = "colore", length = 20)
    @Pattern(regexp = "[a-z]*[A-Z]*")
    private String colore;

    @Column(name = "marca", length = 20)
    @Pattern(regexp = "[a-z]*[A-Z]*")
    private String marca;

    @Column(name = "modello", length = 20)
    private String modello;

    @Column(name = "targa", length = 7, unique = true)
    @Pattern(regexp = "[A-Z]{1}[A-Z]{1}[1-9]{1}[1-9]{1}[1-9]{1}[A-Z]{1}[A-Z]{1}")
    private String targa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idCategoria")
    @JsonbTransient
    private Categoria categoria;


    @OneToMany(mappedBy = "auto")
    @JsonbTransient
    private List<Noleggio> noleggios;

    public Auto() {
    }

    public Auto(Long id, int deleted, String colore, String marca, String modello, String targa, Categoria categoria) {
        this(deleted, colore, marca, modello, targa, categoria);
        this.id = id;
    }

    public Auto(int deleted, String colore, String marca, String modello, String targa, Categoria categoria) {
        this.deleted = deleted;
        this.colore = colore;
        this.marca = marca;
        this.modello = modello;
        this.targa = targa;
        this.categoria = categoria;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getColore() {
        return colore;
    }

    public void setColore(String colore) {
        this.colore = colore;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public String getTarga() {
        return targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public Categoria getCategoria() {
        return categoria;
    }


    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Noleggio> getNoleggios() {
        return noleggios;
    }

    public void setNoleggios(List<Noleggio> noleggios) {
        this.noleggios = noleggios;
    }

    public Noleggio addNoleggio(Noleggio noleggio) {
        getNoleggios().add(noleggio);
        noleggio.setAuto(this);
        return noleggio;
    }

    public Noleggio removeNoleggio(Noleggio noleggio) {
        getNoleggios().remove(noleggio);
        noleggio.setAuto(null);
        return noleggio;
    }
}
