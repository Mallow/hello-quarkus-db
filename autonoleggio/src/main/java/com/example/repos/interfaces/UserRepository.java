package com.example.repos.interfaces;

import com.example.exception.UserNotFoundException;
import com.example.model.Noleggio;
import com.example.model.User;

import javax.ws.rs.WebApplicationException;
import java.util.List;

public interface UserRepository {

    List<User> findAllUser();

    User findUserById(Long id) throws WebApplicationException;

    void updateUser(User user) throws WebApplicationException, UserNotFoundException;

    void createUser(User u);

    void deleteUser(Long userId) throws WebApplicationException, UserNotFoundException;

    User login(String username, String password);

    List<Noleggio> userListNoleggi(String username);

    Long getUserIdByUsername(String username);

    void registrazione(String username, String password, String nome, String cognome);

}
