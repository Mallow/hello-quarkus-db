package com.example.repos.interfaces;

import com.example.model.Categoria;

import javax.ws.rs.WebApplicationException;
import java.util.List;

public interface CategoriaRepository {
    void createCategoria(Categoria cat);

    void updateCategoria(Categoria cat) throws WebApplicationException;

    void deleteCategoria(Long id) throws WebApplicationException;

    List<Categoria> findAllCategorie();

    Categoria findCategoriaById(Long id) throws WebApplicationException;
}
