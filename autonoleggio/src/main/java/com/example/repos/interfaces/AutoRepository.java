package com.example.repos.interfaces;

import com.example.model.Auto;
import com.example.model.Categoria;

import javax.ws.rs.WebApplicationException;
import java.sql.Date;
import java.util.List;

public interface AutoRepository {
    List<Auto> findAllAuto();

    Auto findAutoById(Long id) throws WebApplicationException;

    void updateAuto(Auto auto) throws WebApplicationException;

    void createAuto(Auto a, Categoria c);

    void deleteAuto(Long autoId) throws WebApplicationException;

    List<Auto> autoNoleggiabili(Date inizio, Date fine);
}
