package com.example.repos.interfaces;

import com.example.model.Auto;
import com.example.model.Noleggio;
import com.example.model.User;

import javax.ws.rs.WebApplicationException;
import java.util.Date;
import java.util.List;

public interface NoleggioRepository {
    List<Noleggio> findAllNoleggi(Long userId);

    Noleggio findNoleggioById(Long id) throws WebApplicationException;

    void createNoleggio(Noleggio n, User u, Auto a);

    void updateNoleggio(Noleggio n) throws WebApplicationException;

    void deleteNoleggio(Long id) throws WebApplicationException;

    boolean autoPrenotata(Auto a, Date dataInizio, Date dataFine);

    void addNoleggio(Noleggio n);
}
