package com.example.repos.implementation;

import com.example.exception.UserNotFoundException;
import com.example.model.Noleggio;
import com.example.model.User;
import com.example.repos.interfaces.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.List;

@ApplicationScoped
public class UserRepositoryImp implements UserRepository {

    @Inject
    EntityManager em;


    @Override
    public List<User> findAllUser() {
        return em.createNamedQuery("User.findAll", User.class).getResultList();
    }

    @Override
    public User findUserById(Long id) throws WebApplicationException {
        User u = em.find(User.class, id);
        if (u == null) {
            throw new WebApplicationException("User with id" + id + "not exist", 404);
        }
        return u;
    }

    @Transactional
    @Override
    public void updateUser(User user) throws WebApplicationException, UserNotFoundException {
        User userUpdated = findUserById(user.getId());
        userUpdated.setName(user.getName());
        userUpdated.setSurname(user.getSurname());
        userUpdated.setPassword(user.getPassword());
        userUpdated.setUsername(user.getUsername());
    }

    @Transactional
    @Override
    public void createUser(User u) {
        em.persist(u);
    }

    @Transactional
    @Override
    public void deleteUser(Long userId) throws WebApplicationException, UserNotFoundException {
        em.remove(findUserById(userId));
    }

    @Transactional
    @Override
    public User login(String username, String password) {
        Query q = em.createQuery("SELECT u FROM User u WHERE u.username=:username AND u.password=:password");
        q.setParameter("username", username);
        q.setParameter("password", password);
        return ((User) q.getSingleResult());
    }

    @Override
    public List<Noleggio> userListNoleggi(String username) {
        String s = "SELECT n FROM Noleggio n WHERE n.user.username=:username";
        Query q = em.createQuery(s);
        q.setParameter("username", username);
        return ((List<Noleggio>) q.getResultList());
    }

    @Override
    public Long getUserIdByUsername(String username) {
        String s = "SELECT u FROM User u WHERE u.username=:username";
        Query q = em.createQuery(s);
        User u = ((User) q.getSingleResult());
        return u.getId();
    }

    @Override
    public void registrazione(String username, String password, String nome, String cognome) {
        User u = new User(0, cognome, nome, password, username);
        createUser(u);
    }
}
