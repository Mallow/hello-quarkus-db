package com.example.repos.implementation;

import com.example.model.Auto;
import com.example.model.Noleggio;
import com.example.model.User;
import com.example.repos.interfaces.NoleggioRepository;
import com.example.utility.UtilityFile;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class NoleggioRepositoryImp implements NoleggioRepository {

    @Inject
    EntityManager em;


    @Override
    public List<Noleggio> findAllNoleggi(Long userId) {
        return (List<Noleggio>) em.createNamedQuery("Noleggio.findAll").setParameter("userId", userId).getResultList();
    }

    @Override
    public Noleggio findNoleggioById(Long id) throws WebApplicationException {
        Noleggio n = em.find(Noleggio.class, id);
        if (n == null) {
            throw new WebApplicationException("Categoria with id" + id + "not exist", 404);
        }
        return n;
    }

    @Transactional
    @Override
    public void createNoleggio(Noleggio n, User u, Auto a) {
        n.setUser(u);
        n.setAuto(a);
        em.merge(n);
    }

    @Transactional
    @Override
    public void addNoleggio(Noleggio n) {
        em.merge(n);
    }

    @Transactional
    @Override
    public void updateNoleggio(Noleggio n) throws WebApplicationException {
        Noleggio nUp = findNoleggioById(n.getId());
        nUp.setAuto(n.getAuto());
        nUp.setDataFine(n.getDataFine());
        nUp.setDataInizio(n.getDataInizio());
        nUp.setDataNoleggio(n.getDataInizio());
    }

    @Transactional
    @Override
    public void deleteNoleggio(Long id) throws WebApplicationException {
        em.remove(findNoleggioById(id));
    }

    @Transactional
    @Override
    public boolean autoPrenotata(Auto a, Date dataInizio, Date dataFine) {
        String s = "SELECT n FROM Noleggio n WHERE n.auto=:auto AND n.dataFine>=:dataF AND n.dataInizio<=:dataI";
        Query q = em.createQuery(s);
        q.setParameter("auto", a);
        q.setParameter("dataF", dataFine);
        q.setParameter("dataI", dataInizio);
        try {
            q.getSingleResult();
            return true;
        } catch (NoResultException e) {
            e.printStackTrace();
            return false;
        }
    }
}
