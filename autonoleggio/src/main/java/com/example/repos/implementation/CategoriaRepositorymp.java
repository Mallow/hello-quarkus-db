package com.example.repos.implementation;

import com.example.model.Categoria;
import com.example.repos.interfaces.CategoriaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.List;

@ApplicationScoped
public class CategoriaRepositorymp implements CategoriaRepository {

    @Inject
    EntityManager em;

    @Transactional
    @Override
    public void createCategoria(Categoria cat) {
        em.merge(cat);
    }

    @Transactional
    @Override
    public void updateCategoria(Categoria cat) throws WebApplicationException {
        Categoria catUp = findCategoriaById(cat.getId());
        catUp.setDescrizione(cat.getDescrizione());
        catUp.setPrezzoGiornaliero(cat.getPrezzoGiornaliero());
        catUp.setPrezzoSettimanale(cat.getPrezzoSettimanale());
        catUp.setPrezzoMensile(cat.getPrezzoMensile());
        catUp.setAutos(cat.getAutos());
    }

    @Override
    public void deleteCategoria(Long id) throws WebApplicationException {
        em.remove(findCategoriaById(id));
    }

    @Override
    public List<Categoria> findAllCategorie() {
        return em.createNamedQuery("Categoria.findAll", Categoria.class).getResultList();
    }

    @Override
    public Categoria findCategoriaById(Long id) throws WebApplicationException {
        Categoria c = em.find(Categoria.class, id);
        if (c == null) {
            throw new WebApplicationException("Categoria with id" + id + "not exist", 404);
        }
        return c;
    }


}
