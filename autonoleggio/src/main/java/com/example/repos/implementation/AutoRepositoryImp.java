package com.example.repos.implementation;

import com.example.model.Auto;
import com.example.model.Categoria;
import com.example.repos.interfaces.AutoRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.sql.Date;
import java.util.List;

@ApplicationScoped
public class AutoRepositoryImp implements AutoRepository {

    @Inject
    EntityManager em;

    @Override
    public List<Auto> findAllAuto() {
        return em.createNamedQuery("Auto.findAll", Auto.class).getResultList();
    }

    @Override
    public Auto findAutoById(Long id) throws WebApplicationException {
        Auto a = em.find(Auto.class, id);
        if (a == null) {
            throw new WebApplicationException("Auto with this id = " + id + "not exist in database", 404);
        }
        return a;
    }

    @Transactional
    @Override
    public void updateAuto(Auto auto) throws WebApplicationException {
        Auto autoUp = findAutoById(auto.getId());
        autoUp.setMarca(auto.getMarca());
        autoUp.setColore(auto.getColore());
        autoUp.setCategoria(auto.getCategoria());
        autoUp.setTarga(auto.getTarga());
        autoUp.setModello(auto.getModello());
    }

    @Transactional
    @Override
    public void createAuto(Auto a, Categoria c) {
        a.setCategoria(c);
        em.merge(a);
    }

    @Transactional
    @Override
    public void deleteAuto(Long autoId) throws WebApplicationException {
        em.remove(findAutoById(autoId));
    }

    @Transactional
    @Override
    public List<Auto> autoNoleggiabili(Date inizio, Date fine) {
        String s = "SELECT * FROM auto WHERE id NOT IN(SELECT idAuto FROM noleggio WHERE (dataInizio>=? and dataInizio<=?) or (dataFine>=? AND dataFine<=?) or (noleggio.dataInizio<=? AND noleggio.dataFine>=?)) AND cancellato=0";
        Query q = em.createNativeQuery(s, Auto.class);
        return (List<Auto>) q.getResultList();
    }
}
