package com.example.repos;

import com.example.exceptions.UserException;
import com.example.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.jws.soap.SOAPBinding;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

@ApplicationScoped
public class UserRepository {
    List<User> userList = new ArrayList<User>();
    int idIterator = 0;

    @PersistenceContext
    EntityManager em;

    public List<User> findAllUser() {
        return em.createQuery("select u from User u", User.class).getResultList();
    }

    public int getNextUserId() {
        return idIterator++;
    }

   /* public User findUserById(Long id) {
        for (User u : userList) {
            if (u.getId().equals(id)) {
                return u;
            }
        }
        throw new UserException("user not found");
    }*/

    @Transactional
    public void updateUser(User u) {
        User usrUpdated = findUserById(u.getId());
        usrUpdated.setName(u.getName());
        usrUpdated.setSurname(u.getSurname());
    }

    @Transactional
    public User findUserById(Long id) {
        User u = em.find(User.class, id);
        if(u == null){
            throw new WebApplicationException("user id not found", 404);
        }
        return u;
    }

    @Transactional
    public void addMassive(){
        List<User> mUser = new ArrayList<>();
        mUser.add(new User("Luciano", "Degni"));
        mUser.add(new User("Stefano", "Indelicato"));
        mUser.add(new User("Emanuele", "Pirola"));
        mUser.add(new User("Jessica", "Grimaldi"));
        for(User u : mUser){
            createUser(u);
        }
    }

    @Transactional
    public void createParam(String name, String surname){
        User nUser = new User(name, surname);
        createUser(nUser);
    }

    @Transactional
    public void createUser(User u) {
        em.persist(u);
    }


    @Transactional
    public void deleteUser(Long userId){
        User u = findUserById(userId);
        em.remove(u);
    }
}
