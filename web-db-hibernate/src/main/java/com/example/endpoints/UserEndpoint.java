package com.example.endpoints;


import com.example.model.User;
import com.example.repos.UserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/user")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class UserEndpoint {
    @Inject
    UserRepository userRepository;


    @GET
    public List<User> getAll() {
        return userRepository.findAllUser();
    }

    @POST
    public Response create(String name, String surname) {
        userRepository.createParam(name, surname);
        return Response.status(201).build();
    }

    @POST
    public Response create(User u){
        userRepository.createUser(u);
        return Response.status(201).build();
    }

    @POST
    public Response create() {
        userRepository.addMassive();
        return Response.status(201).build();
    }

    @PUT
    public Response update(User u) {
        userRepository.updateUser(u);
        return Response.status(204).build();
    }

    @DELETE
    public Response delete(@QueryParam("id") Long userId) {
        userRepository.deleteUser(userId);
        return Response.status(204).build();
    }

}