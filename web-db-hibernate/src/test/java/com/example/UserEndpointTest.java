package com.example;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonObject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

@QuarkusTest
public class UserEndpointTest {
    @Test
    public void testUserService(){


       //test @GET
        given()
                .when().get("/user")
                .then()
                .statusCode(200)
                //all'inizio il mio body ha size 0
                //se uso import.sql body ah size numero inserimenti = 1
                .body("$.size()", is(2));


        //cro oggetto json per il post

        JsonObject obj = Json.createObjectBuilder()
                .add("name", "Francesco")
                .add("surname", "totti")
                .build();

        //test @POST
        given()
                .contentType("application/json")
                .body(obj.toString())
                .when()
                .post("/user")
                .then()
                .statusCode(201);

        JsonObject objUpdate = Json.createObjectBuilder()
                .add("id", new Long(1))
                .add("name", "Francesco")
                .add("surname", "totti")
                .build();

        //test @PUT
        given()
                .contentType("application/json")
                .body(objUpdate.toString())
                .when()
                .put("/user")
                .then()
                .statusCode(204);

        /*//test @GET
        given()
                .when().get("/user?userId=1")
                .then()
                .statusCode(200)
                .body(containsString("Francesco"));*/

        //test @DELETE
        given()
                .contentType("application/json")
                .when().delete("/user?id=1")
                .then()
                .statusCode(204);
    }
}
